;
; GriddedMIDIFilter.asm
; MIDI Filter
; For ATTiny10
;  PIN 1 - MIDI in from optocoupler
;  PIN 2 - GND
;  PIN 3 - MIDI out
;  PIN 4 - Debug pulse out for scopes
;  PIN 5 - Vcc
;  PIN 6 - RESET

; Registers
.def TMP = r16      ; Temp
.def T = r17        ; Tick couter, cycles 0..3
.def RMASK = r18    ; Bit mask for reading bits in
.def RBYTE = r19    ; Byte to read into 
.def WMASK = r20    ; Bit mask for writing bits out
.def WBYTE = r21    ; Byte to be written
.def RSTAT = r22    ; Reading state bits
.def WSTAT = r23    ; Writing state bits
.def TMEM = r24     ; Tick memory. Stores snapshots of T in bits
                    ; 0..2, and another in bits 3..5. Bit 6 is also
					; also the 'read completed' flag, to signal a complete
					; byte has been read and can now be written.
					; bits 0..2 are the read time, the value of T
.def WENABLE = r25  ; Write enable.

.equ mtim = 60      ; Number of cycles between ticks

; States 
.equ wait = 0        ; Waiting (for a start bit while reading
                     ; or for a byte to output when writing)
.equ start = 1       ; Reading or writing a start bit + initialisation
.equ active = 2      ; Reading or writing a data bit
.equ stop = 3        ; Reading or writing stop bit + finalisation

.CSEG
.ORG 0
rjmp main   ; Reset interrupt
reti
reti
reti
reti
rjmp timma  ; Timer A interrupt vector
reti
reti
reti
reti
reti


; Replace with your application code
main:
    ; set up the stack
	ldi r16, high(RAMEND)
	out SPH, r16
	ldi r16, low(RAMEND)
	out SPL, r16

	;set up clock divider
	ldi r16, 0x00 ; clock divided by 1
	ldi r17, 0xD8 ; the key for CCP
	out CCP, r17 ; Configuration Change Protection, allows protected changes
	out CLKPSR, r16 ; sets the clock divider

	;set up input / output ports
	; Bit 0 / PIN 1 - MIDI IN
	; Bit 1 / PIN 3 - MIDI OUT
	; Bit 2 / PIN 4 - Debug out
	; Bit 3 / PIN 6 - RESET
	ldi TMP, 0b0110 ;
	out DDRB, TMP ; data direction
	ldi TMP, 0x00 ; sets all pins low
	out PORTB, TMP

	;ldi r16, 0x00
	;out PCICR, r16; Pin change interrupt enable
	;ldi r16, 0x01
	;out PCMSK, r16; Pin change mask on pin 1

	; Enable timer A interrupt
	ldi TMP, 0xFF
	out TIFR0, TMP

	; Timer0 output compare A enable
	ldi TMP, 0b0000_0010
	out TIMSK0, TMP

	ldi TMP, 0x0
	out TCCR0A, TMP
	
	ldi TMP, 0b0000_1001 ; CS0[2:0] == 1 (timer prescaler = CLK/1)
				         ; WGM02 == 1 (Time clear on compare (CTC))
	out TCCR0B, TMP

	; Output compare A to set off timer at regular intervals
	ldi r17, 0x00
	ldi r16, mtim
	out OCR0AH, r17
	out OCR0AL, r16

	ldi r16, 1
	ldi WMASK, 0
	ldi RMASK, 1
	
	ldi WBYTE, 0
	ldi RBYTE, 0

	ldi WENABLE, -1

	ldi WSTAT, 1
	ldi RSTAT, 1
	ldi T, 0
	sei  ; Global interrupt enable
	
loop:
	rjmp loop  ; Do nothing, wait for interrupts.


; Timer A interrupt. Called 4 times per MIDI bit, tracked by variable T
; which cycles 0,1,2,3,0... etc, each one is a 'tick'.
; Everything starts from here, all write actions are taken first,
; All read actions are taken immediately after on the same tick.
; States in RSTAT and WSTAT, and T values stored in TMEM control
; which actions fire on each tick.
timma:
	;sbi portb, 1
	;cbi portb, 1
	inc T        ; T++
	sbrs T, 2    ; If T < 3
	rjmp _tima2  ;   goto _tima2
	ldi T, 0     ; else T = 0

_tima2:
	
	sbrc WSTAT, wait       
	rjmp _wwait

	; Extract lower 3 bits of TMEM, and compare with T
	mov r16, TMEM
	andi r16, 0b00000111
	cp r16, T              ; If T == TMEM_LOW, time to write
	brne _read			   ; Otherwise just read.

	; According to WSTAT:
	sbrc WSTAT, start
	rjmp _wstart           ; Write start bit, or..

	sbrc WSTAT, active
	rjmp _dowrite          ; write data bit, or..

	sbrc WSTAT, stop
	rjmp _wstop            ; write stop bit.
	;reti

_read:
    sbrc RSTAT, wait
	rjmp _rwait            ; Waiting for MIDI in

	; Extract bits 3-5 of TMEM and compare
	mov r16, TMEM
	andi r16, 0b00111000
	lsr r16    ; Shift them back down.
	lsr r16
	lsr r16
	cp r16, T
	brne _finished   ; Stop if not time to read.

	; According to RSTAT:
	sbrc RSTAT, start
	rjmp _rstart        ; Read start bit, or..

	sbrc RSTAT, active
	rjmp _doread        ; Read data bit, or ...

	sbrc RSTAT, stop
	rjmp _rstop         ; Read stop bit.
	reti

_finished:
	reti

_wwait:
	sbrs TMEM, 6  ; Test the 'read complete' flag
	rjmp _read
	
_wready:                   
   	andi TMEM, 0b10111000  ; Clear 'read complete' in bit 6, and clear TMEM_LOW
	or TMEM, T             ; Store T in TMEM_LOW
	cpi WENABLE, 0
	breq _read
	brlt _nodec
	subi WENABLE, 1

_nodec:
	lsl WSTAT              ; Change state waiting -> start
	rjmp _read             ; Write tasks done, goto read. ; Should we just skip to wstart?

_wstart:         ; Write start bit
	cbi portb, 1           ; Set MIDI out low
	lsl WSTAT              ; Change state start -> active
	ldi WMASK, 1           ; Initialise bit mask to 1st bit.
	rjmp _read             ; Write tasks done, goto read.

_wstop:          ; Write stop bit
	sbi portb, 1           ; 
	ldi WSTAT, 1           ; 
	rjmp _read             ; Write tasks done, goto read.

_rwait:                    ; watch for start bit
	sbic PINB, 0
	reti                   ; If not seen, nothing more to do until next interrupt.

	mov TMP, T             ; Edge of start bit seen.
	subi TMP, 2            ; To hit the start bit and subsequent bits in the middle, 
	brge _nomod            ; we need to read again in 2 ticks (4 ticks per bit)
	subi TMP, -4           ; Add 4 to ensure 0<TMP<4

_nomod:
	lsl RSTAT              ; Change state wait -> start
	lsl r16                ; Shift TMP up to write it into TMEM_HIGH
	lsl r16
	lsl r16
	andi TMEM, 0b11000111  ; Wipe TMEM_HIGH
	or TMEM, r16           ; Write TMP into TMEM_HIGH. TMP = (T - 2) % 4
	reti                   ; End of read for this tick


_rstart:    ; This tick we are in the middle of the start bit.
	lsl RSTAT    ; Change state start -> active
	ldi RMASK, 1 ; Initialise bit mask
	reti         ; End of read for this tick

_doread:    ; Read a data bit
    sbic PINB, 0      ; sample the MIDI input pin
	or RBYTE, RMASK   ; Write RMASK into RBYTE if the input pin was 1
	lsl RMASK         ; Move mask to next bit
	brcs _rfinal      ; if the lsl overflowed, we've done 8 bits.
	reti              ; End of read tasks for this tick.

_rstop:     ; This tick we are in the middle of the stop bit.
    ldi RSTAT, 1      ; Go back to wait state to watch for next start bit edge
	reti

_rfinal:
	mov WBYTE, RBYTE ; Copy byte over ready to write out.
	ldi RBYTE, 0     ; Clear RBYTE
	ori TMEM, 64     ; Set flag in TMEM to signal a byte has been read, ready to write out.
	lsl RSTAT        ; Change state active->stop
	cpi WBYTE, 0xF8  ; System real time - always allow, even if WENABLE is 0
	brsh _filter1
	cpi WBYTE, 0xF0  ; System common - go back to WENABLE
	brsh _allow
	cpi WBYTE, 0xA0  ; Everything else beside notes - WENABLE = 0
	brsh _filter
	cpi WBYTE, 0x80  ; Notes - go back to WENABLE
	brsh _allow
	reti             ; else return. Data bytes less than 0x80 all handled according to current WENABLE value.

_filter1:  ; Swallow this one byte no matter what.
	andi TMEM, 0b1011_1111     ; Unset flag in TMEM to signal a byte has been read
	reti

_filter:  ; Byte matches our filter, disable write for this and all data bytes that follow.
	;ldi WBYTE, 61
	ldi WENABLE, 0 ; disable write until next status byte
	sbi portb, 2  ; Pulse the debug line
	cbi portb, 2
	reti

_allow: ; Byte is a status we want to allow, and all data that follows.
	ldi WENABLE, -1 ; enable write
	reti

; force not used at the moment, but this is what would eventually be used
; to allow system realtime messages even in the middle of a suppressed message.
_force: ; Byte must be let through, even if write is disabled.
	cpi WENABLE, 0
	brne _finished
	ldi WENABLE, 1  ; enable write for just one byte
	reti

_dowrite:
	cpi WMASK, 0    ; If WMASK == 0:
	breq _wfinal    ;  WMASK shifted above MSB, nothing to do
	                ; else:
	mov TMP, WBYTE  ;  Copy WBYTE
	and TMP, WMASK  ;  isolate just the bit to write
	breq _zerow     ;  If 0, write a 0, else write a 1

_onew:
	sbi portb, 1   ; Set MIDI output pin high
	rjmp _wfinal

_zerow:
	cbi portb, 1   ; Set MIDI output pin low
	rjmp _wfinal

_wfinal:  ; we've written a bit. 
	lsl WMASK   ; shift to next bit in mask
	breq _wdone ; if no bits left, end or write stop
	rjmp _read

_wdone: ; just after writing last bit
	lsl WSTAT   ; Change state active -> stop
	rjmp _read

